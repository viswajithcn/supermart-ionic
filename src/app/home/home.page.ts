import { Component } from '@angular/core';
import { HomeService } from './home.service';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
  providers:[HomeService]
})
export class HomePage {
  categories;
  title = 'Daily Mart';

  constructor(private api:HomeService){}
  
  ngOnInit(){
    this.getCategories();
  }
  
  getCategories=()=>{
    this.api.getCategories().subscribe(data=>{
        this.categories=data;
      },
      error=>{
        console.log(error);
      }
    )
  }

}
