import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

    baseurl="https://woocommerce-398050-1278553.cloudwaysapps.com/wp-json/wc/v3";

    consumerKey='ck_3834a1e92fa0e91bb5d8371e48c8c3961ffa24d1';
    consumerSecret="cs_1577c9370076e60a1801943b08d054b4d6572538";
    constructor(private http: HttpClient) { }

    getProducts(){
      return this.http.get(this.baseurl+"/products?consumer key=" + this.consumerKey + "&consumer secret=" + this.consumerSecret);
    }
    getCategories(){
      return this.http.get(this.baseurl+"/products/categories?consumer key=" + this.consumerKey + "&consumer secret=" + this.consumerSecret + "&per_page=100&parent=0");
    }

    getll() {
      return this.http.get(this.baseurl);
    }
}   
